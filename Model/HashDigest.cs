﻿namespace Model
{
    public class HashDigest
    {
        public string PreSharedKey { get; set; }
        public string MerchantID { get; set; }
        public string Password { get; set; }
        public string StatusCode { get; set; }
        public string Message { get; set; }
        public string PreviousStatusCode { get; set; }
        public string PreviousMessage { get; set; }
        public string CrossReference { get; set; }
        public string AddressNumericCheckResult { get; set; }
        public string PostCodeCheckResult { get; set; }
        public string CV2CheckResult { get; set; }
        public string ThreeDSecureAuthentication { get; set; }
        public string FraudProtectionCheckResult { get; set; }
        public string CardType { get; set; }
        public string CardClass { get; set; }
        public string CardIssuer { get; set; }
        public string CardIssuerCountryCode { get; set; }
        public string CardNumberFirstSix { get; set; }
        public string CardNumberLastFour { get; set; }
        public string CardExpiryDate { get; set; }
        public string Amount { get; set; }
        public string DonationAmount { get; set; }
        public string CurrencyCode { get; set; }
        public string OrderID { get; set; }
        public string TransactionType { get; set; }
        public string TransactionDateTime { get; set; }
        public string OrderDescription { get; set; }
        public string LineItemSalesTaxAmount { get; set; }
        public string LineItemSalesTaxDescriptio { get; set; }
        public string LineItemQuantity { get; set; }
        public string LineItemAmount { get; set; }
        public string LineItemDescription { get; set; }
        public string CustomerName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Address4 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string CountryCode { get; set; }
        public string EmailAddress { get; set; }
        public string PhoneNumber { get; set; }
        public string DateOfBirth { get; set; }
        public string ShippingName { get; set; }
        public string ShippingAddress1 { get; set; }
        public string ShippingAddress2 { get; set; }
        public string ShippingAddress3 { get; set; }
        public string ShippingAddress4 { get; set; }
        public string ShippingCity { get; set; }
        public string ShippingState { get; set; }
        public string ShippingPostCode { get; set; }
        public string ShippingCountryCode { get; set; }
        public string ShippingEmailAddress { get; set; }
        public string ShippingPhoneNumber { get; set; }
        public string PrimaryAccountName { get; set; }
        public string PrimaryAccountNumber { get; set; }
        public string PrimaryAccountDateOfBirth { get; set; }
        public string PrimaryAccountPostCode { get; set; }
    }
}
