﻿using Model;
using System.Security.Cryptography;
using System.Text;

namespace Service
{
    public class HashService
    {
        private readonly object propInfo;

        public string preHashed(HashDigest hashDigest)
        {
            string h = string.Empty;
            var Properties= hashDigest.GetType().GetProperties();
            for(int iP=0; iP< Properties.Length; iP++)
            {
                object tryGet = Properties[iP].GetValue(hashDigest, null);
                if(tryGet !=null && !string.IsNullOrEmpty(tryGet.ToString()))
                {
                    if (iP != 0)
                    {
                        h += "&";
                    }
                    h+= Properties[iP].Name+"="+tryGet.ToString();
                }
            }
            return h;
        }
        public string paymentHash(Payment payment)
        {
            string h = string.Empty;
            var Properties = payment.GetType().GetProperties();
            for (int iP = 0; iP < Properties.Length; iP++)
            {
                if (!Properties[iP].Name.Equals("HashDigest") && !Properties[iP].Name.Equals("MerchantID"))
                {
                    object tryGet = Properties[iP].GetValue(payment, null);
                    if (tryGet != null && !string.IsNullOrEmpty(tryGet.ToString()))
                    {
                        if (iP != 0)
                        {
                            h += "&";
                        }
                        h += Properties[iP].Name + "=" + tryGet.ToString();
                    }
                    else
                    {
                        if (iP != 0)
                        {
                            h += "&";
                        }
                        h += Properties[iP].Name + "=" + "";
                    }
                }
            }
            return h;
        }
        public string sha1Encrypt(string toEncryp)
        {
            using (SHA1Managed sha1 = new SHA1Managed())
            {
                var hash = sha1.ComputeHash(Encoding.UTF8.GetBytes(toEncryp));
                var sb = new StringBuilder(hash.Length * 2);

                foreach (byte b in hash)
                {
                    sb.Append(b.ToString("x2"));
                }

                return sb.ToString();
            }
        }
    }
}
