﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Service
{
    public class Request
    {       
        public async Task<string> post<T>(string url, T item) where T:Dictionary<string,string>
        {
            try
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.DefaultRequestHeaders.Add("User-Agent", "telnet");
                    var keyValue = item.ToArray<KeyValuePair<string, string>>();
                    var content = new FormUrlEncodedContent (keyValue);
                    HttpResponseMessage response = await httpClient.PostAsync(
                        url,
                        content
                    );
                    if ((int)response.StatusCode != 200)
                        throw new Exception("Erro no webservice");
                    string json = await response.Content.ReadAsStringAsync();
                    return json;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }
}
