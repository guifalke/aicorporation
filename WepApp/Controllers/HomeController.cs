﻿using Model;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace WepApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            HashDigest hashDigest = new HashDigest();
            hashDigest.MerchantID = "Progra-7702818";
            hashDigest.Password = "G6CH6Z90I2";
            hashDigest.PreSharedKey = "5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=";
            ViewData["hashPre"] = new HashService().preHashed(hashDigest);
            return View(hashDigest);
        }
        public async Task<ActionResult> Send(Payment pay)
        {
            string preslected = "PreSharedKey=5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=&MerchantID=Progra-7702818&Password=G6CH6Z90I2&Amount=1000&CurrencyCode=USD&EchoAVSCheckResult=false&EchoCV2CheckResult=false&EchoThreeDSecureAuthenticationCheckResult=false&EchoFraudProtectionCheckResult=&EchoCardType=false&EchoCardNumberFirstSix=&EchoCardNumberLastFour=&EchoCardExpiryDate=&EchoDonationAmount=&AVSOverridePolicy=PPPP&CV2OverridePolicy=PP&ThreeDSecureOverridePolicy=false&OrderID=Order-1234&TransactionType=SALE&TransactionDateTime=2019-05-03 09:07:32 +00:00&DisplayCancelButton=&CallbackURL=http://www.somedomain.com/callback.php&OrderDescription=A test order&LineItemSalesTaxAmount=&LineItemSalesTaxDescription=&LineItemQuantity=&LineItemAmount=&LineItemDescription=&CustomerName=A Customer&DisplayBillingAddress=&Address1=1 Some Street&Address2=&Address3=&Address4=&City=Some City&State=&PostCode=PO54 3DD&CountryCode=0&EmailAddress=test@test.com&PhoneNumber=12345678&DateOfBirth=&DisplayShippingDetails=&ShippingName=&ShippingAddress1=&ShippingAddress2=&ShippingAddress3=&ShippingAddress4=&ShippingCity=&ShippingState=&ShippingPostCode=&ShippingCountryCode=&ShippingEmailAddress=&ShippingPhoneNumber=&CustomerNameEditable=&EmailAddressEditable=false&PhoneNumberEditable=false&DateOfBirthEditable=&CV2Mandatory=false&Address1Mandatory=false&CityMandatory=false&PostCodeMandatory=false&StateMandatory=false&CountryMandatory=false&ShippingAddress1Mandatory=&ShippingCityMandatory=&ShippingPostCodeMandatory=&ShippingStateMandatory=&ShippingCountryMandatory=&ResultDeliveryMethod=POST&ServerResultURL=&PaymentFormDisplaysResult=&ServerResultURLCookieVariables=&ServerResultURLFormVariables=&ServerResultURLQueryStringVariables=&PrimaryAccountName=&PrimaryAccountNumber=&PrimaryAccountDateOfBirth=&PrimaryAccountPostCode=&Skin=&PaymentFormContentMode=&BreakoutOfIFrameOnCallback=";
            string hashedTest = new HashService().sha1Encrypt(preslected);
            HashDigest hashDigest = new HashDigest();
            hashDigest.MerchantID = "Progra-7702818";
            hashDigest.Password = "G6CH6Z90I2";
            hashDigest.PreSharedKey = "5XNm04UE0/EaXDSeKa4Fw29hTzieAl32uO4=";
            string toPost = string.Empty;
            toPost += new HashService().preHashed(hashDigest);
            toPost += new HashService().paymentHash(pay);
            string hashed = new HashService().sha1Encrypt(toPost);
            pay.HashDigest = hashed;
            Dictionary<string, string> formInput = pay.GetType().GetProperties().ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(pay, null)==null?
                    "":(string.IsNullOrEmpty(propInfo.GetValue(pay, null).ToString())?"": propInfo.GetValue(pay, null).ToString())
            );
            //string ok=await new Request().post<Dictionary<string, string>>("https://mms.payvector.net/Pages/PublicPages/PaymentForm.aspx", formInput);
            return View(formInput);
        }

    }
}